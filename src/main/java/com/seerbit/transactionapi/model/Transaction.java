package com.seerbit.transactionapi.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public record Transaction(String id, BigDecimal amount, LocalDateTime timeStamp) { }
