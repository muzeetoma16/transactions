package com.seerbit.transactionapi.model.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum TransactionCreationStatus {
  SUCCESS(201),
  OLDER_THAN_30_SEC(204),
  INVALID_JSON(400),
  NON_PARSABLE_FIELD(422),

  GENERAL_PROCESSING_ERROR(300);

  private final int statusCode;
}
