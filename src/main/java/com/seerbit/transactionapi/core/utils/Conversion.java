package com.seerbit.transactionapi.core.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Conversion {

  public static final DateTimeFormatter dateTimeFormatter
      = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

  public static BigDecimal toBigDecimal(String value) {
    if (isNullOrEmptyOrBlank(value)) {
      return null;
    }

    try {
      return new BigDecimal(value);
    } catch (NumberFormatException e) {
      log.info(":: Amount Value saved cannot be converted to BigDecimal::");
      return null;
    }
  }

  public static LocalDateTime toLocalDateTime(String value) {
    if (isNullOrEmptyOrBlank(value)) {
      return null;
    }

    try {
      return LocalDateTime.parse(value, dateTimeFormatter);
    } catch (Exception e) {
      log.info(":: TimeStamp Value saved cannot be converted to LocalDateTime ::");
      return null;
    }
  }

  public static boolean isOlderThan30Seconds(LocalDateTime dateTime) {
    LocalDateTime thirtySecondsAgo = LocalDateTime.now().minusSeconds(30);
    return dateTime.isBefore(thirtySecondsAgo);
  }

  public static boolean dateIsInFuture(LocalDateTime dateTime) {
    return dateTime.isAfter(LocalDateTime.now());
  }

  private static boolean isNullOrEmptyOrBlank(String value) {
    return value == null || value.isEmpty() || value.isBlank();
  }

  public static BigDecimal convertToAndRound(double value){
    return BigDecimal.valueOf(value).setScale(2, RoundingMode.UP);
  }

}
