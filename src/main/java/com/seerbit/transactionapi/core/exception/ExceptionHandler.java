package com.seerbit.transactionapi.core.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandler {

  @org.springframework.web.bind.annotation.ExceptionHandler(value = { BadRequestException.class })
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ResponseDto<?> badRequestExceptionHandler(BadRequestException ex) {
    ResponseDto<?> responseDto = new ResponseDto<>();
    responseDto.setStatusCode(HttpStatus.BAD_REQUEST.value());
    return responseDto;
  }
}
