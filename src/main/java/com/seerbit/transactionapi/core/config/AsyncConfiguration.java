package com.seerbit.transactionapi.core.config;

import com.seerbit.transactionapi.core.exception.BadRequestException;
import java.lang.reflect.Method;
import java.util.concurrent.Executor;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.AsyncConfigurerSupport;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;


@Configuration
@EnableAsync
public class AsyncConfiguration extends AsyncConfigurerSupport {


	private static final String taskExecutorName = "taskExecutor";
	private static final String taskExecutorPrefix = "taskExecutor-";

	@Bean(name = taskExecutorName)
	public Executor newTaskExecutor() {
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(10);
		executor.setMaxPoolSize(100);
		executor.setQueueCapacity(50);
		executor.setThreadNamePrefix(taskExecutorPrefix);
		return executor;
	}

	static class AsyncExceptionHandler implements AsyncUncaughtExceptionHandler {

		@Override
		public void handleUncaughtException(Throwable ex, Method method,
				Object... params) {
			System.out.println("Unexpected asynchronous exception at : "
					+ method.getDeclaringClass().getName() + "." + method.getName() + ex);

			throw new BadRequestException(ex.getLocalizedMessage());
		}

	}

}
