package com.seerbit.transactionapi.core.config;

import jakarta.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.TimeZone;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;

@Slf4j
@Configuration
public class AppStartup {

  @PostConstruct
  public void initialize() {
    log.info("\n\n ============================ APPLICATION LAUNCHED ======================= \n\n");

    // Set the default timezone
    TimeZone.setDefault(TimeZone.getTimeZone("Africa/Lagos"));

    log.info("Current Time: " + LocalDateTime.now());
  }
}
