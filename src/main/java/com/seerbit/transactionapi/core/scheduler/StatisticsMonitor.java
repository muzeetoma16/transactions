package com.seerbit.transactionapi.core.scheduler;

import static com.seerbit.transactionapi.core.utils.Conversion.convertToAndRound;

import com.seerbit.transactionapi.controller.dto.TransactionStatistics;
import com.seerbit.transactionapi.model.Transaction;
import com.seerbit.transactionapi.repository.TransactionRepo;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
public class StatisticsMonitor {

  private final TransactionRepo transactionRepo;

  /**
   Ensure that the statistics object is updated every 30 seconds so that the
   TransactionService.getStatistics() is called in constant time
   */

  @Async
  @Scheduled(cron = "*/30 * * * * *") // Runs every 30 seconds
  public void updateStatistics() {

    Set<Transaction> transactions30SecAgo = transactionRepo
        .getTransactionByLast30Seconds();

    log.info("Transaction 30 seconds ago "+transactions30SecAgo);

    if(transactions30SecAgo==null){
      transactionRepo.updateTransactionStatistics(new TransactionStatistics());
      return;
    }

    double sum = transactions30SecAgo.stream()
        .mapToDouble(transaction -> transaction.amount().doubleValue())
        .sum();

    long count = transactions30SecAgo.size();

    Optional<Double> max = transactions30SecAgo.stream()
        .map(transaction -> transaction.amount().doubleValue())
        .max(Double::compare);

    Optional<Double> min = transactions30SecAgo.stream()
        .map(transaction -> transaction.amount().doubleValue())
        .min(Double::compare);

    double avg = count > 0 ? sum / count : 0.0;

    TransactionStatistics transactionStatistics
        = new TransactionStatistics(
            convertToAndRound(sum), convertToAndRound(avg),
            convertToAndRound(max.orElse(0.0)),
            convertToAndRound(min.orElse(0.0)), count);

    transactionRepo.updateTransactionStatistics(transactionStatistics);
  }
}
