package com.seerbit.transactionapi.repository;

import com.seerbit.transactionapi.controller.dto.TransactionStatistics;
import com.seerbit.transactionapi.model.Transaction;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;

@Component
public class TransactionRepo {

  private static final Set<Transaction> transactions = Collections.synchronizedSet(new HashSet<>());

  private static TransactionStatistics TRANSACTION_STATISTICS = new TransactionStatistics();

  public synchronized void addTransaction(Transaction transaction) {
    transactions.add(transaction);
  }

  public Set<Transaction> getTransactionByLast30Seconds() {
    LocalDateTime thirtySecondsAgo = LocalDateTime.now().minusSeconds(30);
    synchronized (transactions) {
      if (!transactions.isEmpty()) {
        return transactions.stream()
            .filter(transaction ->  transaction.timeStamp().isAfter(thirtySecondsAgo))
            .collect(Collectors.toSet());
      }
    }
    return null;
  }

  public void deleteAllTransactions() {
    synchronized (transactions) {
      transactions.clear();
    }
  }

  public void updateTransactionStatistics(TransactionStatistics transactionStatistics) {
    TRANSACTION_STATISTICS = transactionStatistics;
  }

  public TransactionStatistics getTransactionStatistics(){
    return TRANSACTION_STATISTICS;
  }
}
