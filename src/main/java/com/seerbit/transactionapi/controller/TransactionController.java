package com.seerbit.transactionapi.controller;


import com.seerbit.transactionapi.controller.dto.TransactionRequest;
import com.seerbit.transactionapi.controller.dto.TransactionStatistics;
import com.seerbit.transactionapi.model.enums.TransactionCreationStatus;
import com.seerbit.transactionapi.service.TransactionService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RequiredArgsConstructor
@RestController
@RequestMapping(path = "/api/transaction")
public class TransactionController {

  private final TransactionService transactionService;

  @PostMapping(value = "")
  public ResponseEntity<?> create(@RequestBody TransactionRequest transactionRequest) {
    TransactionCreationStatus tnxStatus
        = transactionService.createTransaction(transactionRequest);
    return new ResponseEntity<>(HttpStatus.valueOf(tnxStatus.getStatusCode()));
  }

  @GetMapping(value = "/statistics")
  public ResponseEntity<?> getStatistics() {
    TransactionStatistics transactionStatistics
        = transactionService.getTransactionStatistics();
    return new ResponseEntity<>(transactionStatistics,HttpStatus.OK);
  }

  @DeleteMapping(value = "")
  public ResponseEntity<?> delete() {
    transactionService.deleteTransactions();
    return new ResponseEntity<>(HttpStatus.valueOf(204));
  }
}
