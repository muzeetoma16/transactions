package com.seerbit.transactionapi.controller.dto;

public record TransactionRequest(String amount,String timeStamp) { }
