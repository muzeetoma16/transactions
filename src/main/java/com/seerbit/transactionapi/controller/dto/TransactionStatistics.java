package com.seerbit.transactionapi.controller.dto;

import java.math.BigDecimal;

public record TransactionStatistics(BigDecimal sum, BigDecimal avg, BigDecimal max, BigDecimal min, Long count) {
  public TransactionStatistics() {
    this(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, 0L);
  }
}
