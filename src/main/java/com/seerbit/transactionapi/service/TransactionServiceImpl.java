package com.seerbit.transactionapi.service;


import com.seerbit.transactionapi.controller.dto.TransactionRequest;
import com.seerbit.transactionapi.controller.dto.TransactionStatistics;
import com.seerbit.transactionapi.core.config.TransactionConstants;
import com.seerbit.transactionapi.core.scheduler.StatisticsMonitor;
import com.seerbit.transactionapi.core.utils.Conversion;
import com.seerbit.transactionapi.model.Transaction;
import com.seerbit.transactionapi.model.enums.TransactionCreationStatus;
import com.seerbit.transactionapi.repository.TransactionRepo;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService{

  private final TransactionRepo transactionRepo;
  private final StatisticsMonitor statisticsMonitor;

  @Override
  public TransactionCreationStatus createTransaction(TransactionRequest transactionRequest) {

    CompletableFuture<TransactionCreationStatus> createTransaction
        = CompletableFuture.supplyAsync(() -> {

      BigDecimal amount = Conversion.toBigDecimal(transactionRequest.amount());
      LocalDateTime timeStamp = Conversion.toLocalDateTime(transactionRequest.timeStamp());

      if (timeStamp == null || amount == null) {
        return TransactionCreationStatus.NON_PARSABLE_FIELD;
      }
      if (Conversion.dateIsInFuture(timeStamp)) {
        log.info(":: TimeStamp Value is in the future ::");
        return TransactionCreationStatus.NON_PARSABLE_FIELD;
      }
      if (Conversion.isOlderThan30Seconds(timeStamp)) {
        log.info(":: TimeStamp Value is older than 30 sec ::");
        return TransactionCreationStatus.OLDER_THAN_30_SEC;
      }

      String id = UUID.randomUUID().toString();

      Transaction transaction = new Transaction(id, amount, timeStamp);

      transactionRepo.addTransaction(transaction);
      statisticsMonitor.updateStatistics();
      log.info(":: Transaction saved " + transaction + " ::");

      return TransactionCreationStatus.SUCCESS;
    });

    try {
      return createTransaction.get(TransactionConstants.SERVICE_TIME_OUT, TimeUnit.MILLISECONDS);
    } catch (Exception e) {
      log.info(":: Transaction Failed with error " + e.getLocalizedMessage() + " ::");
      return TransactionCreationStatus.GENERAL_PROCESSING_ERROR;
    }
  }

  /**
   *This would return in constant time since the
   * hard work is done every 15_SECONDS on thread
   */
  @Override
  public TransactionStatistics getTransactionStatistics() {
    return transactionRepo.getTransactionStatistics();
  }

  @Override
  public void deleteTransactions() {
     transactionRepo.deleteAllTransactions();
  }
}
