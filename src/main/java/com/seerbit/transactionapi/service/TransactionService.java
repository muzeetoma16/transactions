package com.seerbit.transactionapi.service;

import com.seerbit.transactionapi.controller.dto.TransactionRequest;
import com.seerbit.transactionapi.controller.dto.TransactionStatistics;
import com.seerbit.transactionapi.model.enums.TransactionCreationStatus;

public interface TransactionService {

  TransactionCreationStatus createTransaction(TransactionRequest transactionRequest);

  TransactionStatistics getTransactionStatistics();

  void deleteTransactions();
}
