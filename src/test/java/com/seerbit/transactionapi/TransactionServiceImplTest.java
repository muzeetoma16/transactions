package com.seerbit.transactionapi;

import static com.seerbit.transactionapi.core.utils.Conversion.dateTimeFormatter;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.seerbit.transactionapi.controller.dto.TransactionRequest;
import com.seerbit.transactionapi.controller.dto.TransactionStatistics;
import com.seerbit.transactionapi.core.scheduler.StatisticsMonitor;
import com.seerbit.transactionapi.model.Transaction;
import com.seerbit.transactionapi.model.enums.TransactionCreationStatus;
import com.seerbit.transactionapi.repository.TransactionRepo;
import com.seerbit.transactionapi.service.TransactionServiceImpl;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class TransactionServiceImplTest {

  private TransactionServiceImpl transactionService;

  @Mock
  private TransactionRepo transactionRepo;

  @Mock
  private StatisticsMonitor statisticsMonitor;

  @BeforeEach
  void setUp() {
    MockitoAnnotations.openMocks(this);
    transactionService = new TransactionServiceImpl(transactionRepo,statisticsMonitor);
  }

  @Test
  void createTransaction_WithValidRequest_ReturnsSuccessStatus() {
    TransactionRequest request = new TransactionRequest(BigDecimal.TEN.toString(),
        getCurrentTime());

    TransactionCreationStatus status = transactionService.createTransaction(request);

    assertEquals(TransactionCreationStatus.SUCCESS, status);
    verify(transactionRepo, times(1)).addTransaction(any(Transaction.class));
  }

  @Test
  void createTransaction_WithNonParsableField_ReturnsNonParsableFieldStatus() {
    TransactionRequest request = new TransactionRequest(null, null);

    TransactionCreationStatus status = transactionService.createTransaction(request);

    assertEquals(TransactionCreationStatus.NON_PARSABLE_FIELD, status);
    verify(transactionRepo, never()).addTransaction(any(Transaction.class));
  }

  @Test
  void createTransaction_WithFutureTimeStamp_ReturnsNonParsableFieldStatus() {
    LocalDateTime futureTimeStamp = LocalDateTime.now().plusDays(1);
    TransactionRequest request =
        new TransactionRequest(BigDecimal.TEN.toString(), futureTimeStamp.toString());

    TransactionCreationStatus status = transactionService.createTransaction(request);

    assertEquals(TransactionCreationStatus.NON_PARSABLE_FIELD, status);
    verify(transactionRepo, never()).addTransaction(any(Transaction.class));
  }

  @Test
  void createTransaction_WithOlderTimeStamp_ReturnsOlderThan30SecStatus() {
    LocalDateTime olderTimeStamp = LocalDateTime.now().minusSeconds(31);
    TransactionRequest request
        = new TransactionRequest(BigDecimal.TEN.toString(), getCurrentTime(olderTimeStamp));

    TransactionCreationStatus status = transactionService.createTransaction(request);

    assertEquals(TransactionCreationStatus.OLDER_THAN_30_SEC, status);
    verify(transactionRepo, never()).addTransaction(any(Transaction.class));
  }

  @Test
  void getTransactionStatistics_ReturnsTransactionStatisticsFromRepo() {
    TransactionStatistics expectedStatistics = new TransactionStatistics();
    when(transactionRepo.getTransactionStatistics()).thenReturn(expectedStatistics);

    TransactionStatistics statistics = transactionService.getTransactionStatistics();

    assertEquals(expectedStatistics, statistics);
    verify(transactionRepo, times(1)).getTransactionStatistics();
  }

  private String getCurrentTime(){
    LocalDateTime currentTime = LocalDateTime.now();
    return currentTime.format(dateTimeFormatter);
  }

  private String getCurrentTime(LocalDateTime currentTime){
    return currentTime.format(dateTimeFormatter);
  }
}
