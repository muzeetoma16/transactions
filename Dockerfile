FROM openjdk:17-jdk-alpine

WORKDIR /app

COPY target/transaction-api-0.0.1-SNAPSHOT.jar .

CMD ["java", "-jar", "transaction-api-0.0.1-SNAPSHOT.jar"]
